pub enum Command {
    Init,
}

impl Command {
    pub fn parse(string: String) -> Result<Command, &'static str> {
        return match string.as_str() {
            "init" => Ok(Command::Init),
            _ => Err("Unknown command"),
        };
    }
}
