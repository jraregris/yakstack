use super::command::Command;

pub struct Config {
    pub command: Command,
}

impl Config {
    pub fn new(mut args: std::env::Args) -> Result<Config, &'static str> {
        args.next();

        let first_arg = args.next().ok_or("No command given");

        return Command::parse(first_arg?).map(|command| Config { command });
    }
}
