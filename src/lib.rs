use std::error::Error;

mod command;
use command::Command;

pub mod config;
use config::Config;

pub fn init() -> () {
    println!("INININININIT!")
}

pub fn run(config: Config) -> Result<(), Box<dyn Error>> {
    match config.command {
        Command::Init => init(),
    }
    Ok(())
}
