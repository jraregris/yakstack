#!/bin/bash


echo ""
echo " -- YAKSTACK TCR -- "
echo ""

# provide commit msg if passed
if [ -z "$1" ]
then
    commit_cmd="git commit -v"
else
    commit_cmd="git commit -m '$1'"
fi

function tezt {
    cargo test && cargo build
}

function commit {
    git add -A && $commit_cmd && git push
}

function revert {
    git reset --hard
}

function untracked_files {
    git status --porcelain | grep "??"
}

untracked_files && echo "UNTRACKED FUCKING FILES!" && exit 1

# TEST
tezt

# COMMIT
[ $? -eq 0 ] && commit

# REVERT
[ $? -eq 0 ] || revert
